---
title: Архив
subtitle: «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page
lang: ru
ref: Архив
show_sidebar: true
menubar: menu
image: /img/background04.jpg
hero_image: /img/background04.jpg
hero_height: is-small
description: Архив
permalink: /archive.html
---

***

## 2019 ##   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="{{site.baseurl}}/3032019.html"><img src="{{site.baseurl}}/img/archive/30-3-2019.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="{{site.baseurl}}/3032019.html">2019 №3(30)</a></p>
  </div>
  <div class="column is-centered">

  </div>
</div>
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="{{site.baseurl}}/2812019.html"><img src="{{site.baseurl}}/img/archive/28-1-2019.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="{{site.baseurl}}/2812019.html">2019 №1(28)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="{{site.baseurl}}/2922019.html"><img src="{{site.baseurl}}/img/archive/29-2-2019.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="{{site.baseurl}}/2922019.html">2019 №2(29)</a></p>
  </div>
</div>
## 2018 ##   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="{{site.baseurl}}/2412018.html"><img src="{{site.baseurl}}/img/archive/24-1-2018.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="{{site.baseurl}}/2412018.html">2018 №1(24)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="{{site.baseurl}}/2522018.html"><img src="{{site.baseurl}}/img/archive/25-2-2018.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="{{site.baseurl}}/2522018.html">2018 №2(25)</a></p>
  </div>
</div>
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="{{site.baseurl}}/2632018.html"><img src="{{site.baseurl}}/img/archive/26-3-2018.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="{{site.baseurl}}/2632018.html">2018 №3(26)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="{{site.baseurl}}/2742018.html"><img src="{{site.baseurl}}/img/archive/27-4-2018.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="{{site.baseurl}}/2742018.html">2018 №4(27)</a></p>
  </div>
</div>
### 2017 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/20-1-2017.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2017 №1(20)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/21-2-2017.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2017 №2(21)</a></p>
  </div>
</div>
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/22-3-2017.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2017 №3(22)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/23-4-2017.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2017 №4(23)</a></p>
  </div>
</div>
### 2016 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/18-1-2016.png" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2016 №1(18)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/19-2-2016.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2016 №2(19)</a></p>
  </div>
</div>
### 2013 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/16-1-2013.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2013 №1(16)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/17-2-2013.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2013 №2(17)</a></p>
  </div>
</div>
### 2012 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/14-1-2012.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2012 №1(14)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/15-2-2012.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2012 №2(15)</a></p>
  </div>
</div>
### 2011 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/12-1-2011.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2011 №1(12)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/13-2-2011.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2011 №2(13)</a></p>
  </div>
</div>
### 2010 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/10-1-2010.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2010 №1(10)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/11-2-2010.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2010 №2(11)</a></p>
  </div>
</div>
### 2009 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/08-1-2009.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2009 №1(8)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/09-2-2009.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2009 №2(9)</a></p>
  </div>
</div>
### 2008 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/06-1-2008.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2008 №1(6)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/07-2-2008.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2008 №2(7)</a></p>
  </div>
</div>
### 2007 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/04-1-2007.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2007 №1(4)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/05-2-2007.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2007 №2(5)</a></p>
  </div>
</div>
### 2006 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/02-1-2006.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2006 №1(2)</a></p>
  </div>
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/03-2-2006.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2006 №2(3)</a></p>
  </div>
</div>
### 2005 ###   
<div class="columns is-vcentered">
  <div class="column is-centered">
    <p class="level-item has-text-centered"><a href="https://kirillkirillov.github.io/vestnik/products/2018%20%E2%84%963(26)/"><img src="{{ site.baseurl }}/img/archive/01-1-2005.jpg" /></a></p>
    <p class="bd-notification is-primary has-text-centered"><a href="#">2005 №1(1)</a></p>
  </div>
