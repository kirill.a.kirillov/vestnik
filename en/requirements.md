---
title: Requirements to Manuscripts of Scientific Articles
subtitle: Журнал «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page-en
lang: en
ref: Требования к статьям
show_sidebar: true
menubar: menu-en
image: /img/background-2.jpg
hero_image: /img/background-2.jpg
hero_height: is-small
description: Requirements to Manuscripts of Scientific Articles
permalink: /en/requirements.html
---

***  

**Article text** has to contain following elements: the problem statement in general terms and its connection with important scientific and practical tasks; the analysis of last achievements and publications where the solution of the stated problem has been started, the emphasis of the unresolved earlier parts of the general problem discussed in the article; the formulation of the article objective; the exposition of the main research material with full explanation of obtained scientific results; conclusions and prospects of further researches in this direction.
Articles are subject to publication in the journal, originality of the main text of which is at least 70%, when checked in the Anti-Plagiarism system.

**In the Editorial Board Are Submitted:**   

* an article;
* an abstract in Russian (size – 2000 characters) with key words;
* an expert opinion;
* a cover letter (with indication that  the article has not been published before);
* information about authors with indication of: surname, name and patronymic, academic title, academic degree, position, place of employment, contact phone number (mobile communication is obligatory), е-mail.

**Manuscript Design**   

* Materials are submitted on sheets of the A4 size.
* Fields are specular: inside and outside – 20 mm, head and bottom – 25 mm.
* Font: Times New Roman, 12 pt.
* Line-to-line spacing – single.
* Article size – 5–10 pages.
* References on literary sources are indicated in square brackets in the mentioning order.
* Formulae are typed in the equation editor MS Equation – 3.0 or in later version. Numbers are given in brackets with alignment on the right edge. Formulae numbering is within the article. Style: variable is typed in italics; vector- matrix – in bold, font Times New Roman, Greek symbols – in regular font. Dimensions: main characters – 12 pt; large index – 7 pt; small index – 5 pt; large character – 18 pt; small character – 12 pt. It is forbidden to perform formulae with the help of MathCAD or other similar programs.
* Figures are placed after mentioning in the text. Screened illustrations, dashed graphic objects, graphs, diagrams are given in *.wmf, *.jpg, *.tif formats. These illustrations are saved additionally in the form of separate files. While using *.jpg, *.tif formats permitting capability should be 300 – 600 dpi. It is forbidden to make figures in MS Word. It is forbidden to embed graphic materials in the form of objects connected with other programs, for example with COMPASS, MS Excel, etc.
* Tables are made in MS Word and they should be located on one page without hyphenation. Table headings include the number within the article and the title. Tables are placed after reference in the text.
* Bibliography. References should be actual: the list of literature should contain at least 8 references not older than 10 years, 3 of them published over the past 5 years.
* Among the sources should be no more than 5 documents, the author or co-author of which is the author himself.
* The list should preferably include documents whose texts are posted on the Internet.
* The bibliography is compiled in the order the documents are mentioned in the text, it is performed in accordance with the State Standard 7.0.100-2018 «Bibliographic record. Bibliographic description.General requirements and rules of compilation».   

**Manuscript Should Contain:**   

* UDC;
* surname, name, patronymic of authors published in one paragraph, separated by commas, without hyphenation, with the indication of the academic degree;
* information about authors: organization, city, country, codes of scientometric databases (RISC, SPIN-code; SCOPUS, ORCID), е-mail;
* article title;
* annotation – maximum 5 lines. Font: Times New Roman, 10 pt, italic;
* article text;
* bibliography.

Fee for the article publication is not paid. Payment for the manuscript publication is not collected.   

**Editorial board address:**   
Automobile-Highway Institute of Donetsk National Technical University, 51, Kirov St., Gorlovka, 84646.

<span class="icon"><i class="fas fa-phone"></i></span> Contact phone number: +38 (071) 412-79-07, +38 (071) 331-45-58   

<span class="icon"><i class="fas fa-envelope-open-text"></i></span> Е-mail: vestnik-adi@adidonntu.ru   

<span class="icon"><i class="fab fa-internet-explorer"></i></span> Website: http://vestnik.adidonntu.ru/   
