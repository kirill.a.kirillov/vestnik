---
title: «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
subtitle: international scientific-technical periodical<br>ISSN 1990-7796
layout: page-en
lang: en
ref: «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
show_sidebar: true
menubar: menu-en
image: /img/background.jpg
hero_image: /img/background.jpg
hero_height: is-small
description: About journal
permalink: /en/index.html
---

***

«Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute» is an international scientific-technical periodical, its pages reflect main results of the research work of scientists, graduate students, doctoral candidates, students of higher education establishments and research organizations.

**Founder**   
Automobile-Highway Institute of State Higher Education Establishment «Donetsk National Technical University».

**Publisher**   
Automobile-Highway Institute of Donetsk National Technical University

**Year of foundation**   
2004

**ISSN 1990-7796**

**Frequency**   
four times per year

**Sphere of distribution**   
national and foreign

**Journal is indexed in abstract and bibliographic databases:**
* Google Scholar;
* RISC (eLIBRARY.RU)

Journal is reviewing and publishing materials in the form of articles, short reports, overviews in technical and economic sciences.

**Subject items**
* Transport
* Highway Construction and Maintenance
* Environmental Protection
* Economics and Management

**Branches of science:**
* technical sciences,
* economic sciences.

**Specialty groups of scientists:**
* 05.04.00 Power, Metallurgical, Chemical Machine Building
* 05.22.00 Transport
* 05.23.00 Civil Engineering and Architecture
* 08.00.00 Economic Sciences

**Specialties of scientists:**
* 05.04.02 Heat Engines
* 05.22.01 Transport and Technology Systems of the country, its regions and cities, Production Organization on Transport
* 05.22.08 Transportation Process Management
* 05.22.10 Automobile Transport Maintenance
* 05.23.05 Building Materials and Products
* 05.23.11 Design and Construction of Highways, Underground Railroads, Airfields, Bridges and Transport Tunnels
* 05.23.19 Ecological Safety of Construction and Municipal Services
* 08.00.05 Economics and National Economy Management (by branches of activity including Economics, Organization and Management of Enterprises, Branches, Complexes; Innovation Management, Regional Labour Economics, Population and Demography Economics; Nature Management Economics, Business Economics, Marketing; Management, Pricing, Economic Security, Standardization and Product Quality Control, Land Utilization, Recreation and Tourism).   
* 08.00.13 Mathematical and Instrumental Methods of Economics 
