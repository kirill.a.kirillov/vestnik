---
title: Rules of submission, reviewing and publication of scientific articles
subtitle: Журнал «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page-en
lang: en
ref: Правила направления, рецензирования и опубликования научных статей
show_sidebar: true
menubar: menu-en
image: /img/background.jpg
hero_image: /img/background.jpg
hero_height: is-small
description: Rules of submission, reviewing and publication of scientific articles
permalink: /en/rules.html
---

***   

* Journal «Bulletin of Automobile-Highway Institute of Donetsk National Technical University» is reviewing all received materials related to the journal subject with the purpose of their expert judgement. All reviewers are respected authorities in the subject of reviewing materials and have publications in the subject of reviewing materials in recent three years. The original reviews are kept by the publisher for five years.
* Articles are receivable if they strictly meet requirements to author original articles and have all accompanying documents.

Articles are submitted to journal editorial staff by e-mail: vestnik-adi@adidonntu.ru   
Article manuscripts and originals of all necessary documents are sent to the address: 51, Kirov St., Gorlovka, Donetsk region, 284646, Publishing Department.

* During seven days the head of the publishing department informs authors about article receipt and originals of all necessary documents by e-mail and submits an article to the editorial board. 
* Editorial board determines the compliance of the article with the journal specialization, requirements to the design and submits it to the specialist for reviewing, doctor or candidate of sciences with scientific specialization similar to the article subject. The reviewer cannot be the author or co-author of the reviewed manuscript.
* Terms of reviewing in each individual case are determined with a glance of conditions for article publishing as prompt as possible.
* The reviewer should evaluate a manuscript according to the five-point scale on the following parameters: scientific novelty, scientific validity of results, significance of results, consistency and lucidity of material exposition, quality of the manuscript design.
 
To give total sum of points. To state comments on each point, additional remarks, suggestions and general reasoning of the conclusion.  To indicate specifically what it is necessary to improve. 

To make a conclusion: «Recommended insistently for publication», «Recommended for publication», «Recommended for publication conditionally (a manuscript can be accepted for publication only if it would be appropriate improved by authors) », «Not recommended for publication».

* Articles are reviewed anonymously. The article review is submitted to the author without signature and indication of the name, position, place of employment of the reviewer. The breach of anonymity can be possible in case of the author’s consent, declaration of the reviewer about plagiary or falsification of materials stated in the article. 
* If the review contains recommendations on article improvement, a person responsible for an issue submits a review to the author with the suggestion to take into account recommendations in new article variant or to disprove recommendations reasonably. The article worked over by the author iteratively is submitted for reviewing.
* If the review is positive the final decision about the publication appropriateness is taken by the editorial board.
* In the journal «Bulletin of Automobile-Highway Institute of Donetsk National Technical University» restriction on the possible number of articles of the same author in one journal issue is accepted – up to two articles.
* All obtained materials are inspected in the system «Antiplagiat». Minimum originality limit of the text taken for reviewing is 70%. In case of discordance inspection record is sent to the author to conform the text to the given requirements.
* Editorial board informs authors about the decision making about the article publication.
* In case of the manuscript discordance to the journal requirements editorial staff send authors review copies or motivated refusal and also undertake to submit review copies to the Ministry of Education and Science when requested.
