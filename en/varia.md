---
title: Other
subtitle: Журнал «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page-en
lang: en
ref: Разное
show_sidebar: true
menubar: menu-en
image: /img/background.jpg
hero_image: /img/background.jpg
hero_height: is-small
description: Other
permalink: /en/varia.html
---

***

Sample of the certificate of publication of the article (PDF)
