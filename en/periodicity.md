---
title: Issue Frequency
subtitle: Журнал «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page-en
lang: en
ref: Периодичность издания
show_sidebar: true
menubar: menu-en
image: /img/background.jpg
hero_image: /img/background.jpg
hero_height: is-small
description: Issue Frequency
permalink: /en/periodicity.html
---

***

Journal is issued four times per year. 
