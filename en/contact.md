---
title: Сontacts
subtitle: Журнал «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page-en
lang: en
ref: Контакты
show_sidebar: true
menubar: menu-en
image: /img/background.jpg
hero_image: /img/background.jpg
hero_height: is-small
description: Сontacts
permalink: /en/contact.html
---

***

**Automobile-Highway Institute of Donetsk National Technical University**   
<i class="fas fa-address-card"></i>&emsp;51, Kirov St., Gorlovka, 84646.

**Gumeniuk Mikhail Mikhailovich**   
Cand.of Tech.Sc., Assoc. Prof., Assoc.Prof. of the Chair «Mathematical Modelling» - Executive Secretary.   
Chair «Mathematical Modelling»   
<i class="fas fa-phone"></i>&emsp;Phone: +38 (071) 412-79-07   
<i class="fas fa-envelope-open-text"></i>&emsp;E-mail: <misha_gumenyuk@mail.ru>   

**Kurgan Natalia**   
Head of the Publishing department – in charge of the issue.   
Publishing Department, room 1-104.   
<i class="fas fa-phone"></i>&emsp;Phone: +38 (071) 331-45-58   
<i class="fas fa-envelope-open-text"></i>&emsp;E-mail: <vestnik-adi@adidonntu.ru>, <druknf@rambler.ru>  

<figure class="image is-16by9">
  <iframe class="has-ratio" width="640" height="360" src="https://yandex.ua/map-widget/v1/?um=constructor%3A5b0ce4a62101b0a8cf3b5b2860572756ce2b1e0754a9b7d4beddea60de75cb31&amp;source=constructor" frameborder="0" allowfullscreen></iframe>
</figure>
