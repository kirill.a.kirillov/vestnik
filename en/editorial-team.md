---
title: Editorial Board
subtitle: Журнал «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page-en
lang: en
ref: Редакционная коллегия
show_sidebar: true
menubar: menu-en
image: /img/background-3.jpg
hero_image: /img/background-3.jpg
hero_height: is-small
description: Editorial Board
permalink: /en/editorial-team.html
---

***

### EDITOR-IN-CHIEF: ###   
***Mishchenko Nikolai Ivanovich***   
Doctor of Technical Sciences, Professor, Head of the Chair «Automobile Transport» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
### DEPUTY EDITOR-IN-CHIEF: ###   
***Vovk Leonid***   
Doctor of Technical Sciences, Professor, Head of Chair «Mathematical Modelling» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
### EXECUTIVE SECRETARY: ###   
***Gumeniuk Mikhail***   
Candidate of Economic Sciences, Associate Professor, Associate Professor of Chair «Mathematical Modelling» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
### MEMBERS OF EDITORIAL BOARD: ###   
***Andrienko Vladimir***   
Doctor of Economic Sciences, Professor, Professor of the Chair «Management Information Systems» of the Donetsk National University, Donetsk   
   
***Angelina Irina***   
Doctor of Economic Sciences, Professor, Head of the Tourism Chair of the Donetsk National University of Economics and Trade Named After Mikhail Tugan-Baranovskiy, Donetsk   
   
***Bratchun Valerii***   
Doctor of Technical Sciences, Professor, Head of Chair «Highways and Airfields» of Donbas National Academy of Civil Engineering and Architecture, Makeyevka   
   
***Drozd Gennadii***   
Doctor of Technical Sciences, Professor, Head of Chair Department of Industrial, Civil Engineering and Architecture at the Institute of Construction, Architecture, Housing and Communal  Services of Lugansk National University named after V.Dahl, Lugansk   
   
***Lepa Roman***   
Doctor of Economic Sciences, Professor, Chief Research Officer at the «Modelling Economic Systems», Economic Research Institute, Donetsk   
   
***Melnikova Elena***   
Doctor of Technical Sciences, Professor, Head of Chair «Management of Organizations» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
***Nasonkina Nadezhda***   
Doctor of Technical Sciences, Professor, Head of Chair «Urban Construction and Municipal Services» of Donbas National Academy of Civil Engineering and Architecture, Makeyevka   
   
***Orobinskii Vladimir***   
Doctor of Agricultural Sciences, Professor, Dean of Agroengineering Departmentof Voronezh State Agricultural University N.A. Emperor Peter the Great, Voronezh, Russia   
   
***Polovian Aleksei***   
Minister of the Ministry of Economic Development of the Donetsk People’s Republic, acting Head of the Chair «Management» of Donetsk National University, Donetsk   
   
***Poluianov Vladimir***   
Doctor of Economic Sciences, Professor, Professor of Chair «Business and Design Technologies» of Don Cossak State Institute of Food Technologies and Economics (branch) of Moscow State University of Technologies and Management named after K.G. Razumovskiy (First Cossak University)», Rostov-on-Don, Russia   
   
***Pukhov Evgenii***   
Doctor of Technical Sciences, Professor, Head of Chair «Maintenance of Transport and Technological Machines» of Voronezh State Agricultural University N.A. Emperor Peter the Great, Voronezh, Russia   
   
***Solntsev Aleksei***   
Candidate of Technical Sciences, Professor, Head of the Chair «Operation and Servicing of Motor Vehicles» of the Moscow Automobile and Road Construction State Technical University (MADI), Moscow   
   
***Silianov Valentin***   
Doctor of Technical Sciences, Professor, Professor of the Chair «Road Survey and Design» of Moscow Automobile and Road Construction State Technical University (MADI), Vice-Rector for EMA work, first Deputy Chairman of Educational and Methodical Association of Russian Ministry of Education in the field of transport machines and technological complexes, Moscow   
   
***Timokhin Vladimir***   
Doctor of Economic Sciences, Professor, Professor of the Chair «Economical Cybernetics» of the Donetsk National University, Donetsk   
   
***Khomenko Iana***   
Doctor of Economic Sciences, Professor, Professor of the Chair «Economics and Public Administration», Donetsk National Technical University, Donetsk   
   
***Chistiakov Igor***   
Doctor of Technical Sciences, Professor, Head of Chair «Airports, Engineering Geology and Geotechnics», Dean of Road Building Department of Moscow Automobile and Road Construction State Technical University (MADI), Moscow, Russia   
   
***Shatrov Mikhail***   
Doctor of Technical Sciences, Professor, Head of Chair «Heat Engineering and Automotive Engines» of Moscow Automobile and Road Construction State Technical University (MADI), Moscow, Russia   
   
***Bykov Vareii***   
Candidate of Technical Sciences, Dean of the Faculty «Road and Transport» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Bashevaia Tatiana***   
Candidate of Technical Sciences, Associate Professor, Head of the Chair «Technosphere Safety» of Donbas National Academy of Civil Engineering and Architecture, Makeyevka   
   
***Guba Viktoriia***   
Candidate of Technical Sciences, Associate Professor, Associate Professor of the Chair «Highways and Artificial Strucutures» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Dudnikov Aleksandr***   
Candidate of Technical Sciences, Associate Professor, Head of Chair «Transport Technologies» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
***Dudnikova Natalia Nikolaevna***   
Candidate of Technical Sciences, Associate Professor, Associate Professor of the Chair «Transport Technologies» of theAutomobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Zaglada Roman***   
Candidate of Economic Sciences, Associate Professor, Аcting Director of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Karpinets Antonina***   
Candidate of Chemical Sciences, Associate Professor, Associate Professor of the Chair «General Engineering Disciplines» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Kurgan Elena***   
Candidate of Economic Sciences, Associate Professor, Associate Professor of Chair «Management and Business Law» of Donetsk National Technical University, Donetsk   
   
***Konovalchik Maksim***   
Candidate of Technical Sciences, Associate Professor of the Chair «Ecology and Social Safety» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Legkii Sergei***   
Candidate of Economic Sciences, Associate Professor, Associate Professor of the Chair «Transport Technologies» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Likhacheva Viktoriia Viktorovna***   
Candidate of Technical Sciences, Associate Professor,Head of the Chairof the Chair «Ecology and Social Safety» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Morozova Ludmila***   
Candidate of Technical Sciences, Associate Professor, Associate Professor of Chair «Highways and Artificial Structures» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
***Nikolaenko Vladimir***   
Candidate of Technical Sciences, Associate Professor, Associate Professor of the Chair «Mathematical Modelling» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Nikulshin Sergei***   
Candidate of Technical Sciences, Associate Professor, Associate Professor of Chair «Automobile Transport» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
***Samisko Dmitrii***   
Candidate of Technical Sciences, Associate Professor of the Chair «Transport Technologies» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Selezneva Nadezhda***   
Candidate of Economic Sciences, Associate Professor, Associate Professor of Chair «Transport Technologies» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
***Skrypnik Tatiana***   
Candidate of Technical Sciences, Associate Professor, Head of the Chair «Highways and Artificial Structures» of Automobile-Highway Institute of Donetsk National University   
   
***Khimchenko Arkadii***   
Candidate of Technical Sciences, Associate Professor of Chair «Automobile Transport», Head of Research Department of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
***Chornous Oksana***   
Candidate of Economic Sciences, Associate Professor, Associate Professor of the Chair «Organization Management» of the Automobile and Road Institute of Donetsk National Technical University, Gorlovka   
   
***Shilin Igor***   
Candidate of Technical Sciences, Associate Professor, Associate Professor of the Chair «Highways and Artificial Structures» of Automobile-Highway Institute of Donetsk National University, Gorlovka   
   
