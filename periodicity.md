---
title: Периодичность издания
subtitle: Журнал «Вести Автомобильно-дорожного института = Bulletin of the Automobile and Highway Institute»
layout: page
lang: ru
ref: Периодичность издания
show_sidebar: true
menubar: menu
image: /img/background08.jpg
hero_image: /img/background08.jpg
hero_height: is-small
description: Периодичность издания
permalink: /periodicity.html
---

***

Журнал выходит 4 раза в год.
